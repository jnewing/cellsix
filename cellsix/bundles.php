<?php

// bundle configuration

return array(

	'docs' => array('handles' => 'docs'),

    // xml-rpc
    'xmlrpc' => array(
        'autoloads' => array(
            'map' => array(
                'XMLRPC\\XML_RPC' => '(:bundle)/xmlrpc.php',
            ),
        ),
    ),

    // Atheme
    'atheme' => array(
        'autoloads' => array(
            'map' => array(
                'Atheme\\AthemeAPI' => '(:bundle)/athemeapi.php',
            ),
        ),
    ),

    // gravitas
    'gravitas' => array(
        'autoloads' => array(
            'map' => array(
                'Gravitas\\API' => '(:bundle)/api.php',
            ),
        ),
    ),

);