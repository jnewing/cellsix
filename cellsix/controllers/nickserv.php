<?php

/**
 * Copyright (c) 2012, Joseph Newing
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Joseph Newing.
 * 
 * 4. Neither the name of the Joseph Newing nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

class Nickserv_Controller extends Base_Controller
{
    /**
     * __construct()
     */
    public function __construct()
    {
        $this->filter('before', 'auth');
    }

    /**
     * nickserv/index
     */
    public function action_index()
    {
        // get our default user info
        $user_info = \Atheme\AthemeAPI::NickServ('info', Session::get('nickname'))->asString();

        if (Request::method() == 'POST')
            $user_info = \Atheme\AthemeAPI::NickServ('info', Input::get('nickname'))->asString();

        // build a view
        return View::make('nickserv.index')
            ->with('user_info', $user_info);
    }

    /**
     * nickserv/password
     */
    public function action_password()
    {
        // validation rules
        $rules = array(
            'password' => 'required',
            'repassword' => 'required|same:password'
        );

        if (Request::method() == 'POST')
        {
            $validation = Validator::make(Input::all(), $rules);
            if ($validation->fails())
                return Redirect::to('nickserv/password')->with_errors($validation);

            // exec Atheme command
            \Atheme\AthemeAPI::NickServ('set', 'password', Input::get('password'));

            // redirect with success
            return Redirect::to('nickserv/password')->with('success', 'Password has been successfully updated.');
        }

        // build our view
        return View::make('nickserv.password');
    }

    /**
     * nickserv/email
     */
    public function action_email()
    {
        // validation rules
        $rules = array(
            'email' => 'required'
        );

        if (Request::method() == 'POST')
        {
            $validation = Validator::make(Input::all(), $rules);
            if ($validation->fails())
                return Redirect::to('nickserv/email')->with_errors($validation);

            // exec Atheme command
            \Atheme\AthemeAPI::NickServ('set', 'email', Input::get('email'));

            // redirect with success
            return Redirect::to('nickserv/email')->with('success', 'Email has been changed.');
        }

        // build our view
        return View::make('nickserv.email');
    }
    
}