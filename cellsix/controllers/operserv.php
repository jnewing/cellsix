<?php

/**
 * Copyright (c) 2012, Joseph Newing
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Joseph Newing.
 * 
 * 4. Neither the name of the Joseph Newing nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

class Operserv_Controller extends Base_Controller
{
    public function __construct()
    {
        $this->filter('before', 'auth');
    }

    /**
     * hostserv
     */
    public function action_index()
    {	 
        // did the user submit?
        if (Request::method() === 'POST')
        {
            if (Input::get('action') === 'remove')
            {
                $command = \Atheme\AthemeAPI::OperServ('AKILL', 'DEL', Input::get('akill_id'))->asObject();

                // return with message from Atheme
                return Redirect::to('operserv')
                    ->with(($command->success) ? 'success' : 'error', $command->msg);
            }
            else
            {
                if (Input::get('akill_type') == 'Timed')
                    $command = \Atheme\AthemeAPI::OperServ('AKILL', 'ADD', Input::get('nickname'), '!T ' . Input::get('akill_duration') . Input::get('akill_duration_type'), Input::get('reason', 'No Reason Specified'))->asObject();
                else
                    $command = \Atheme\AthemeAPI::OperServ('AKILL', 'ADD', Input::get('nickname'), '!P', Input::get('reason', 'No Reason Specified'))->asObject();

                // return a message from Atheme
                return Redirect::to('operserv')
                    ->with(($command->success) ? 'success' : 'error', $command->msg);
            }
        }

        // get current akills
        $akills = \Atheme\AthemeAPI::OperServ('AKILL', 'LIST')->asAKillList();

    	// return our view
    	return View::make('operserv.akill')
            ->with('akills', $akills);
    }

    /**
     * hostserv/modules
     */
    public function action_modules()
    {
        if (Request::method() === 'POST')
        {   
            // unload a module
            if (Input::get('action') === 'unload')
            {
                $command = \Atheme\AthemeAPI::OperServ('MODUNLOAD', Input::get('unload_module'))->asObject();

                return Redirect::to('operserv/modules')
                    ->with(($command->success) ? 'success' : 'error', $command->msg);
            }

            // load a module
            if (Input::get('action') === 'load')
            {
                $command = \Atheme\AthemeAPI::OperServ('MODLOAD', Input::get('load_module'))->asObject();

                return Redirect::to('operserv/modules')
                    ->with(($command->success) ? 'success' : 'error', $command->msg);
            }
        }

        // get currently loaded modules list
        $modules = \Atheme\AthemeAPI::OperServ('MODLIST')->asModuleList();

        return View::make('operserv.modules')
            ->with('modules', $modules);
    }

    /**
     * hostserv/rehash
     */
    public function action_rehash()
    {
        if (Request::method() === 'POST')
        {
            // rehash the services
            $command = \Atheme\AthemeAPI::OperServ('REHASH')->asObject();

            return Redirect::to('operserv/rehash')
                ->with(($command->success) ? 'success' : 'error', $command->msg);
        }

        return View::make('operserv.rehash');
    }

    /**
     * hostserv/hash
     */
    public function action_hash()
    {
        $hash = false;

        if (Request::method() === 'POST')
        {
            // hash using posix crypt
            if (Input::get('enc_type') === 'CRYPT')
            {
                $salt = substr(uniqid(), 0, 8);
                $hash = '$1$' . $salt . '$' . crypt(Input::get('password') . $salt);
            }

            // hash using md5
            if (Input::get('enc_type') === 'MD5')
                $hash = '$rawmd5$' . md5(Input::get('password'));

            // hash using sha1
            if (Input::get('enc_type') === 'SHA1')
                $hash = '$rawsha1$' . sha1(Input::get('password'));

            if (Input::get('enc_type') === 'SHA256')
                $hash = '$rawsha256$' . hash('sha256', Input::get('password'));
        }

        return View::make('operserv.hash')
            ->with('hash', $hash);
    }

}
