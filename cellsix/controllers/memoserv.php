<?php

/**
 * Copyright (c) 2012, Joseph Newing
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Joseph Newing.
 * 
 * 4. Neither the name of the Joseph Newing nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

class Memoserv_Controller extends Base_Controller
{
    public function __construct()
    {
        $this->filter('before', 'auth');
    }

    /**
     * memoserv/index
     */
    public function action_index()
    {
        // get your current memos (both old and new)
        $memos = \Atheme\AthemeAPI::MemoServ('list')->asMemoList();

        return View::make('memoserv.index')
            ->with('memos', $memos);
    }

    public function action_send()
    {
        // is the user sending a memo?
        if (Request::method() === 'POST')
        {   
            // try and send the memo to the requested user
            $command = \Atheme\AthemeAPI::MemoServ('send', Input::get('nickname'), Input::get('memo'))->asObject();

            // return a message from Atheme
            return Redirect::to('memoserv')
                ->with(($command->success) ? 'success' : 'error', $command->msg);
        }
    }

    /**
     * memoserv/getmemo
     */
    public function action_getmemo($id)
    {   
        // try and get the memo
        $memo = \Atheme\AthemeAPI::MemoServ('read', $id)->asString();

        // return what Atheme said as json (as this should be an Ajax call)
        return Response::json(array(
            'id' => $id,
            'memo' => $memo,
            )
        );
    }

    /**
     * memoserv/forward
     */
    public function action_forward($id)
    {
        // forward on the memo
        $command = \Atheme\AthemeAPI::MemoServ('forward', Input::get('nickname'), $id)->asObject();

        // return the user with a message
        return Redirect::to('memoserv')
            ->with(($command->success) ? 'success' : 'error', $command->msg);
    }

    /**
     * memoserv/delete
     */
    public function action_delete($id)
    {
        // delete the memo
        $command = \Atheme\AthemeAPI::MemoServ('delete', $id)->asObject();
            

        // return the user with a message
        return Redirect::to('memoserv')
            ->with(($command->success) ? 'success' : 'error', $command->msg);
    }

}
