<?php

/**
 * Copyright (c) 2012, Joseph Newing
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Joseph Newing.
 * 
 * 4. Neither the name of the Joseph Newing nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

class Hostserv_Controller extends Base_Controller
{
    public function __construct()
    {
        $this->filter('before', 'auth');
    }

    /**
     * hostserv
     */
    public function action_index()
    {	
    	// did the user submit?
    	if (Request::method() === 'POST')
    	{
    		$command = \Atheme\AthemeAPI::HostServ('take', Input::get('vhost'))->asObject();

    		return Response::json(array(
    			'success' => ($command) ? true : false,
    			'msg' => $command->msg,
    		));

    		// return Redirect::to('hostserv')
    		// 	->with(($command) ? 'success' : 'error', $command->msg);
    	}

    	// get a list of current offerings
    	$hosts = \Atheme\AthemeAPI::HostServ('offerlist')->asHostList();

    	// return our view
    	return View::make('hostserv.offerlist')
    		->with('hosts', $hosts);
    }

    /**
     * hostserv/request
     */
    public function action_request()
    {
    	// did the user submit?
    	if (Request::method() === 'POST')
    	{
    		$command = \Atheme\AthemeAPI::HostServ('request', Input::get('vhost'))->asObject();

    		return Redirect::to('hostserv/request')
    			->with(($command) ? 'success' : 'error', $command->msg);
    	}

    	return View::make('hostserv.request');
    }

    /**
     * hostserv/waiting
     */
    public function action_waiting()
    {
        if (!Session::get('hostserv'))
            return Redirect::to('hostserv');

        if (Request::method() === 'POST')
        {   
            // accept the vhost
            if (Input::get('action') === 'accept')
            {
                $command = \Atheme\AthemeAPI::HostServ('activate', Input::get('nickname'))->asObject();

                return Response::json(array(
                    'success' => ($command->success) ? true : false,
                    'msg' => $command->msg
                ));
            }

            // reject the vhost
            if (Input::get('action') === 'reject')
            {
                $command = \Atheme\AthemeAPI::HostServ('reject', Input::get('nickname'))->asObject();

                return Response::json(array(
                    'success' => ($command->success) ? true : false,
                    'msg' => $command->msg
                ));
            }
        }

        // get current waiting list
        $waiting = \Atheme\AthemeAPI::HostServ('waiting')->asWaitingList();

        return View::make('hostserv.waiting')
            ->with('waiting', $waiting);
    }

}
