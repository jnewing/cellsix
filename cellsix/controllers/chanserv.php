<?php

/**
 * Copyright (c) 2012, Joseph Newing
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Joseph Newing.
 * 
 * 4. Neither the name of the Joseph Newing nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * Chanserv_Contoller Class
 * 
 * @author Joseph Newing (synmufin)
 * @email jnewing [at] gmail [dot] com
 * 
 * notes:
 *
 */

class Chanserv_Controller extends Base_Controller
{
    // chanserv permissions
    // +v - Enables use of the voice/devoice commands.
    // +V - Enables automatic voice.
    // +o - Enables use of the op/deop commands.
    // +O - Enables automatic op.
    // +s - Enables use of the set command.
    // +i - Enables use of the invite and getkey commands.
    // +r - Enables use of the kick, kickban, ban and unban commands.
    // +R - Enables use of the recover and clear commands.
    // +f - Enables modification of channel access lists.
    // +t - Enables use of the topic and topicappend commands.
    // +A - Enables viewing of channel access lists.
    // +F - Grants full founder access.
    // +b - Enables automatic kickban.

    /**
     * construct
     */
    public function __construct()
    {
        $this->filter('before', 'auth');
    }

    /**
     * chanserv
     */
    public function action_index()
    {
        if (Request::method() === 'POST')
        {
            return View::make('chanserv.index')
                ->with('channel_info', \Atheme\AthemeAPI::ChanServ('info', Input::get('channel'))->asString() );
        }

        return View::make('chanserv.index');
    }

    /**
     * chanserv/topic
     */
    public function action_topic()
    {
        if (Request::method() === 'POST')
        {
            // set the topic
            $command = \Atheme\AthemeAPI::ChanServ('topic', Input::get('channel'), Input::get('topic'))->asObject();

            if ($command->success)
                return Redirect::to('chanserv/topic')
                    ->with('success', $command->msg);
            else
                return Redirect::to('chanserv/topic')
                    ->with('error', $command->msg);
        }

        return View::make('chanserv.topic');
    }

    /**
     * chanserv/kick
     */
    public function action_kick()
    {
        if (Request::method() === 'POST')
        {
            // kick the user
            $command = \Atheme\AthemeAPI::ChanServ('kick', Input::get('channel'), Input::get('nickname'), Input::get('reason', 'No reason given.'))->asObject();

            if ($command->success)
                return Redirect::to('chanserv/kick')
                    ->with('success', $command->msg);
            else
                return Redirect::to('chanserv/kick')
                    ->with('error', $command->msg);
        }

        return View::make('chanserv.kick');
    }

    /**
     * chanserv/ban
     */
    public function action_ban()
    {
        if (Request::method() === 'POST')
        {
            // (un)ban the user
            if (Input::get('bantype') == 'ban')
                $command = \Atheme\AthemeAPI::ChanServ('ban', Input::get('channel'), Input::get('nickname'))->asObject();
            else
                $command = \Atheme\AthemeAPI::ChanServ('unban', Input::get('channel'), Input::get('nickname'))->asObject();

            if ($command->success)
                return Redirect::to('chanserv/ban')
                    ->with('success', $command->msg);
            else
                return Redirect::to('chanserv/ban')
                    ->with('error', $command->msg);
        }

        return View::make('chanserv.ban');
    }

    /**
     * chanserv/akick
     */
    public function action_akick()
    {
        // does the user have some form of access
        if (Session::has('access'))
        {
            foreach (Session::get('access') as $chan => $access)
            {
                if (strstr($access, 'A')) // check to see if we have the '+A' flag
                {
                    $akicks[$chan] = \Atheme\AthemeAPI::ChanServ('akick', $chan, 'list')->asEntryList();
                }
            }
        }

        // did the user submit?
        if (Request::method() === 'POST')
        {
            if (Input::get('action') == 'remove') // remove an akick
            {
                $command = \Atheme\AthemeAPI::ChanServ('akick', Input::get('cfchannel'), 'DEL', Input::get('cfnickname'))->asObject();

                return Redirect::to('chanserv/akick')
                    ->with(($command->success) ? 'success' : 'error', $command->msg);
            }
            else    // add a new akick
            {
                $command = \Atheme\AthemeAPI::ChanServ('akick', Input::get('channel'), 'ADD', Input::get('nickname'), Input::get('reason'))->asObject();

                return Redirect::to('chanserv/akick')
                    ->with(($command->success) ? 'success' : 'error' , $command->msg);
            }
        }

        return View::make('chanserv.akick')
            ->with('akicks', $akicks);
    }

    /**
     * chanserv/flags
     */
    public static function action_flags()
    {
        if (Request::method() === 'POST')
        {
            $flags = str_replace(',', '', Input::get('hidden-flags'));
            $command = \Atheme\AthemeAPI::ChanServ('flags', Input::get('channel'), Input::get('nickname'), $flags)->asObject();

            return Redirect::to('chanserv/flags')
                ->with(($command->success) ? 'success' : 'error', $command->msg);
        }
                
        return View::make('chanserv.flags');
    }

}