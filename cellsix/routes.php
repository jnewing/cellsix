<?php

/**
 * default routes
 * routes here are just default routes that we use through out Cell Six
 */
Route::get('/', function()
{
	return Redirect::to('login');
});

/**
 * login & register routes
 * routes are used in the authentication method os Cell Six
 */
Route::get('login', function()
{
    return View::make('login');
});

Route::post('login', function()
{
    // try and login
    if ( ! $login = \Atheme\AthemeAPI::login(Input::get('nickname'), Input::get('password')))
    {
        // log
        Log::write('warning', 'Failed login attempt: ' . Input::get('nickname') . ' - ' . Request::ip());

        // return
        return Redirect::to('login')
            ->with_errors(array('login' => 'Invalid username and/or password.'));
    }

    // build session
    Session::put('authed', TRUE);
    Session::put('authCookie', $login);
    Session::put('nickname', Input::get('nickname'));
    Session::put('access', \Atheme\AthemeAPI::accesslist());
    Session::put('operserv', \Atheme\AthemeAPI::operaccess());
    Session::put('hostserv', \Atheme\AthemeAPI::hostaccess());

    // return
    return Redirect::to('nickserv');
});

Route::get('register', function()
{
    return View::make('register');
});

Route::post('register', function()
{
    $command = \Atheme\AthemeAPI::register(Input::get('nickname'), Input::get('password'), Input::get('email'))->asObject();

    return Redirect::to('login')
        ->with(($command->success) ? 'success' : 'error', $command->msg);
});

Route::get('logout', function()
{
    Session::flush();
    return Redirect::to('login');
});

/**
 * controller routes
 * routes to some default controllers to provide default Cell Six functionality
 */
Route::controller('nickserv');
Route::controller('chanserv');
Route::controller('memoserv');
Route::controller('hostserv');
Route::controller('operserv');

/**
 * 404 & 500 error routes
 * self explaintory, or so I would hope
 */
Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/**
 * route filters
 * a few default route filters we can apply to our calls both before and after calls are made
 */
Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
    // Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
    if ( ! \Atheme\AthemeAPI::check())
    {
        Session::flush();
        return Redirect::to('login')
            ->with_errors(array('login' => 'Session timeout.'));
    }
});