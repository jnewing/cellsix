@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.chanserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Channel Info.</li>
                </ul>
            </div>
        </div>

        @if (isset($channel_info))
            <br />
            <div class="row">
                <div class="span12">
                    <pre>{{ $channel_info }}</pre>
                </div>
            </div>
        @endif

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Channel Info.</h1>
                <p class="lead">Users can look up information on the channel, to see things like who registered it, when and more.</p>
                <hr />
            </div>
        </div>
        
        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Channel Info</legend>
                        <p>Use the form below to issue the info command on a given channel.</p><br />

                        <div class="input-append">
                            <input class="span2" id="appendedInputButton" name="channel" placeholder="#channel" size="25" type="text"><button class="btn" type="submit">Lookup!</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    @endsection