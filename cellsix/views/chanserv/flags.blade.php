@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.chanserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Channel Flags</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Channel Flags</h1>
                <p class="lead">Allows channel owners etc... to manage channel flags.</p>
                <hr />
            </div>
        </div>
        
        <!-- block -->
        <div class="row">
            <!-- form -->
            <div class="span6">
                <form action="" method="post">
                    <div class="form-horizontal well">
                        <legend>Channel Flags</legend>
                        <p>Use the form below to add or remove flags for a user on a specific channel.</p><br />

                        <div class="control-group">
                            <label class="control-label">Channel Name</label>
                            <div class="controls">
                                <input type="text" name="channel" placeholder="#channel" class="input-xlarge" autocomplete="off" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Nickname / Hostmask</label>
                            <div class="controls">
                                <input type="text" name="nickname" placeholder="Nickname" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Flags</label>
                            <div class="controls">
                                <input type="text" name="flags" placeholder="+/-Flags" class="flagManager"/>
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" id="baction" class="btn btn-primary">Modify Flags</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="span6">
                <h2>Flags</h2>
                <span class="label label-info">+v</span> - Enables use of the voice/devoice commands.<br /><br />
                <span class="label label-info">+V</span> - Enables automatic voice.<br /><br />
                <span class="label label-info">+h</span> - Enables use of the halfop/dehalfop commands.<br /><br />
                <span class="label label-info">+H</span> - Enables automatic halfop.<br /><br />
                <span class="label label-info">+o</span> - Enables use of the op/deop commands.<br /><br />
                <span class="label label-info">+O</span> - Enables automatic op.<br /><br />
                <span class="label label-info">+a</span> - Enables use of the protect/deprotect commands.<br /><br />
                <span class="label label-info">+q</span> - Enables use of the owner/deowner commands.<br /><br />
                <span class="label label-info">+s</span> - Enables use of the set command.<br /><br />
                <span class="label label-info">+i</span> - Enables use of the invite and getkey commands.<br /><br />
                <span class="label label-info">+r</span> - Enables use of the kick, kickban, ban and unban commands.<br /><br />
                <span class="label label-info">+R</span> - Enables use of the recover and clear commands.<br /><br />
                <span class="label label-info">+f</span> - Enables modification of channel access lists.<br /><br />
                <span class="label label-info">+t</span> - Enables use of the topic and topicappend commands.<br /><br />
                <span class="label label-info">+A</span> - Enables viewing of channel access lists.<br /><br />
                <span class="label label-info">+F</span> - Grants full founder access.<br /><br />
                <span class="label label-info">+b</span> - Enables automatic kickban.
            </div>
        </div>


        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

                // channel name helper
                $('input[name="channel"]').typeahead({
                    source: function(typeahead, query) {
                        var access = new Array();

                        @if (Session::has('access'))
                            @foreach (Session::get('access') as $chan => $flags)
                                access.push("{{ $chan }}");
                            @endforeach
                        @endif

                        return access;
                    }
                });

                // flags helper
                $(".flagManager").tagsManager();

            });
        </script>

    @endsection