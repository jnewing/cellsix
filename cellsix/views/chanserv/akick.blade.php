@layout('pages.fullpage')

    @section('content')

        <!-- confirmation modal -->
        <div id="cfModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cfModalLabel" aria-hidden="true">
            <form action="" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="cfModalLabel">Are you sure?</h3>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to remove this AKick?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Ooops No!</button>
                    <button type="submit" class="btn btn-primary btn-danger">Yes</button>
                </div>
                <input type="hidden" name="cfchannel" value="" />
                <input type="hidden" name="cfnickname" value="" />
                <input type="hidden" name="action" value="remove" />
            </form>
        </div>

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.chanserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Channel AKick</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Manage AKicks</h1>
                <p class="lead">Manage existing akicks or add new ones using the form below.</p>
                <hr />
            </div>
        </div>
        
        <!-- existing akicks -->
        @if (count($akicks) > 0)
            @foreach ($akicks as $channel => $data)
                @if (count($data) > 0)
                <div class="row">
                    <div class="span12">
                        <h2>{{ $channel }}</h2>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="10">No.</th>
                                    <th width="200">Nick / Host</th>
                                    <th width="350">Reason</th>
                                    <th>Set By</th>
                                    <th width="120">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $entry => $ak)
                                <tr>
                                    <td>{{ $entry }}</td>
                                    <td><span class="label">{{ $ak['host'] }}</span></td>
                                    <td><a href="#">{{ $ak['reason'] }}</a></td>
                                    <td><a href="#">{{ $ak['ext'] }}</a></td>
                                    <td>
                                        <a href="#cfModal" role="button" data-toggle="modal" id="confirm-{{ $entry }}" class="btn btn-danger btn-mini" data-value="{{ $channel }}:{{ $ak['host'] }}"><icon class="icon-remove icon-white"></icon> Remove</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            @endforeach
        @endif

        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Add AKick</legend>
                        <p>Add and akick to a channel you have access to.</p><br />

                        <div class="control-group">
                            <label class="control-label">Channel</label>
                            <div class="controls">
                                <input type="text" name="channel" placeholder="#channel" class="input-xlarge" autocomplete="off" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Nick / Hostmask</label>
                            <div class="controls">
                                <input type="text" name="nickname" placeholder="Nickname / Hostmask" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Reason</label>
                            <div class="controls">
                                <input type="text" name="reason" placeholder="Reason for akick (Optional)" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Add AKick</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

                $('a[id^="confirm"]').click(function(e) {
                    var parts = $(this).attr('data-value').split(':');
                    //$('#cfModal').modal('show');

                    $('input[name="cfchannel"]').val(parts[0]);
                    $('input[name="cfnickname"]').val(parts[1]);
                });

                $('input[name="channel"]').typeahead({
                    source: function(typeahead, query) {
                        var access = new Array();

                        @if (Session::has('access'))
                            @foreach (Session::get('access') as $chan => $flags)
                                access.push("{{ $chan }}");
                            @endforeach
                        @endif

                        return access;
                    }
                });
            });
        </script>

    @endsection
    