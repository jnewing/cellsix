@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.chanserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Ban/Unban User</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Ban/Unban User</h1>
                <p class="lead">If you have channel access you can ban or unban a user.</p>
                <hr />
            </div>
        </div>
        
        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Ban/Unban User</legend>
                        <p>Use the form below to add or remove a channel ban on a user.</p><br />

                        <div class="control-group">
                            <label class="control-label">Channel Name</label>
                            <div class="controls">
                                <input type="text" name="channel" placeholder="#channel" class="input-xlarge" autocomplete="off" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Nickname / Hostmask</label>
                            <div class="controls">
                                <input type="text" name="nickname" placeholder="Nickname" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Reaason</label>
                            <div class="controls">
                                <input type="text" name="reason" placeholder="Reason (Optional)" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Ban or Unban?</label>
                            <div class="controls">
                                <div class="btn-group" data-toggle="buttons-radio">
                                    <button type="button" class="btn active btn-ban">Ban</button>
                                    <button type="button" class="btn btn-unban">Unban</button>
                                </div>
                            </div>
                        </div>


                        <div class="controls">
                            <button type="submit" id="baction" class="btn btn-primary">Add Ban</button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="bantype" value="ban" />
            </form>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {
                $('.btn-ban').click(function() {
                    $('#baction').html('Add Ban');
                    $('input[name="bantype"]').val('ban');
                });

                $('.btn-unban').click(function() {
                    $('#baction').html('Remove Ban');
                    $('input[name="bantype"]').val('unban');
                });

                $('input[name="channel"]').typeahead({
                    source: function(typeahead, query) {
                        var access = new Array();

                        @if (Session::has('access'))
                            @foreach (Session::get('access') as $chan => $flags)
                                access.push("{{ $chan }}");
                            @endforeach
                        @endif

                        return access;
                    }
                });
            });
        </script>

    @endsection