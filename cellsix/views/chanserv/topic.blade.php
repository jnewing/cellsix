@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.chanserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Channel Topic</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Channel Topic</h1>
                <p class="lead">Users can set a channel topic on a channel they have appropriated access to do so.</p>
                <hr />
            </div>
        </div>

        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Set Channel Topic</legend>
                        <p>Use the form below to set the topic on a channel.</p>
                        <div class="control-group">
                            <label class="control-label">Channel Name</label>
                            <div class="controls">
                                <input type="text" name="channel" placeholder="#channel" class="input-xlarge" autocomplete="off" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Topic</label>
                            <div class="controls">
                                <textarea name="topic" placeholder="New channel topic!"></textarea>
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Change topic</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {
                $('input[name="channel"]').typeahead({
                    source: function(typeahead, query) {
                        var access = new Array();

                        @if (Session::has('access'))
                            @foreach (Session::get('access') as $chan => $flags)
                                access.push("{{ $chan }}");
                            @endforeach
                        @endif

                        return access;
                    }
                });
            });
        </script>

    @endsection