@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.chanserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Kick User</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Kick User</h1>
                <p class="lead">If you have channel access you can kick a user.</p>
                <hr />
            </div>
        </div>
        
        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Kick User</legend>
                        <p>Use the form below to kick a user from a channel.</p><br />

                        <div class="control-group">
                            <label class="control-label">Channel Name</label>
                            <div class="controls">
                                <input type="text" name="channel" placeholder="#channel" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Nickname</label>
                            <div class="controls">
                                <input type="text" name="nickname" placeholder="Nickname" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Reaason</label>
                            <div class="controls">
                                <input type="text" name="reason" placeholder="Reason (Optional)" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Kick user</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    @endsection