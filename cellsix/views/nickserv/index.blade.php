@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.nickserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Nickname Lookup</li>
                </ul>
            </div>
        </div>

        @if (isset($user_info))
            <br />
            <div class="row">
                <div class="span12">
                    <pre>{{ print_r($user_info, true) }}</pre>
                </div>
            </div>
        @endif

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Nickname Lookup</h1>
                <p class="lead">View information on registered nicknames within the Atheme database.</p>
                <hr />
            </div>
        </div>
        
        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Info</legend>
                        <p>Use the form below to issue the info command on a given nickname.</p><br />

                        <div class="input-append">
                            <input class="span2" id="appendedInputButton" name="nickname" placeholder="Nickname" size="25" type="text"><button class="btn" type="submit">Lookup!</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    @endsection