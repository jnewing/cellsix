@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.nickserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Change Email</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Change Your Email</h1>
                <p class="lead">Users can change the email associated with their Atheme account.</p>
                <hr />
            </div>
        </div>
        
        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Change Email</legend>
                        <p>Use the form below to change your account email.</p><br />

                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
                                <input type="text" name="email" placeholder="Email@address.com" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Change email</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    @endsection
