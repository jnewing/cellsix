<!doctype html>
<html lang="en">
<head>
	<title>CellSix :: Atheme v4.0</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- css -->
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/bootstrap-responsive.min.css') }}
	{{ HTML::style('css/bootstrap-tagmanager.css') }}
	{{ HTML::style('css/cellsix.css') }}
	
	<!-- js -->
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    {{ HTML::script('js/bootstrap.js') }}
    {{ HTML::script('js/bootstrap-typeahead.js') }}
    {{ HTML::script('js/bootstrap-tagmanager.js') }}
</head>
<body class="main">
