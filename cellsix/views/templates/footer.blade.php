	<!-- footer -->
	<div class="footer">
		
		<!--  top footer -->
		<div class="links">
			<div class="container">
				<div class="row">
					<div class="span2">
						<icon class="footer-icon"></icon>
					</div>

					<div class="span2">
						<ul class="footer-links">
							<li>Get Help
								<ul>
									<li><a href="https://bitbucket.org/jnewing/cellsix/wiki/Home">Documentation</a></li>
									<li><a href="https://bitbucket.org/jnewing/cellsix">CellSix Website</a></li>
									<li><a href="#">{{ Config::get('application.network') }}</a></li>
								</ul>
							</li>
						</ul>
					</div>

					<div class="span2">
						<ul class="footer-links">
							<li>Your Account
								<ul>
									<li><a href="{{ URL::to('nickserv/password') }}">Change Password</a></li>
									<li><a href="{{ URL::to('nickserv/email') }}">Change Email</a></li>
									<li><a href="{{ URL::to('logout') }}">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!--  copyright -->
		<div class="copy">
			<!--//
				// This copyright notice MUST remain intact, and on this footer.blade.php template.
				// under NO circumstance is it ok to remove this. Failure to keep this copyright notice will result in the user (users)
				// agreeing to pay a "Copyright Remove Fee" associated with this software.
				// --> 
			{{ "<p><strong>Cell Six</strong> Web Panel (Copyright &copy; Joseph Newing, 2011 - 2012) [A:" . Config::get('Atheme::default.atheme_bundle_ver') . " X:" . Config::get('XMLRPC::default.xmlrpc_bundle_ver') . " C:" . Config::get('application.cellsix_ver') . "]</p>" }}
		</div>

	</div>
	
</body>
</html>