    <!--  header -->
        <!-- top nav -->
        <div class="navbar navbar-fixed-top top-line">
            <div class="container">
                <!-- logo -->
                <div class="pull-left">
                    <a href="{{ URL::to('nickserv') }}" class="brand">Cell<strong>Six</strong><span class="label label-inverse">v4.0</span></a>
                </div>
                <!-- top right menu -->
                <div class="pull-right">
                    <span class="welcome">
                        Welcome {{ Session::get('nickname') }}, you have {{ \Atheme\AthemeAPI::newmessages() }} new <a href="{{ URL::to('memoserv') }}" class="account-type">messages</a> 
                        <!-- <a href="#" class="push_button orange-on-dark"><icon class="icon-bell icon-brown"></icon>Upgrade</a> -->
                        <span class="divider-topline"></span>
                    </span>
                    <div class="toplinks">
                        My Account <icon class="icon-lock icon-gray"></icon>
                    </div>
                    <span class="divider-topline"></span> 
                    <a href="{{ URL::to('logout') }}" style="color: #797979">Logout <icon class="icon-share-alt icon-gray"></icon></a>
                </div>
            </div>
        </div>

        <!-- main nav -->
        <div class="navbar navbar-blue navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="nav-collapse collapse">
                        <!-- main nav left -->
                        <ul class="nav">
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ ucfirst(Config::get('Atheme::default.atheme_services.nickserv')) }}</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('nickserv') }}">Nickname Info.</a></li>
                                    <li><a href="{{ URL::to('nickserv/password') }}">Change Password</a></li>
                                    <li><a href="{{ URL::to('nickserv/email') }}">Change Email</a></li>
                                </ul>
                            </li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ ucfirst(Config::get('Atheme::default.atheme_services.chanserv')) }}</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('chanserv') }}">Channel Info.</a></li>
                                    <li><a href="{{ URL::to('chanserv/topic') }}">Set Topic</a></li>
                                    <li><a href="{{ URL::to('chanserv/kick') }}">Kick User</a></li>
                                    <li><a href="{{ URL::to('chanserv/ban') }}">Ban User</a></li>
                                    <li><a href="{{ URL::to('chanserv/akick') }}">AKick User</a></li>
                                    <li><a href="{{ URL::to('chanserv/flags') }}">Channel Flags</a></li>
                                </ul>
                            </li>
                            <li class="divider-vertical"></li>
                            <li><a href="{{ URL::to('memoserv') }}">{{ ucfirst(Config::get('Atheme::default.atheme_services.memoserv')) }}</a></li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ ucfirst(Config::get('Atheme::default.atheme_services.hostserv')) }}</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('hostserv') }}">Offer List</a></li>
                                    <li><a href="{{ URL::to('hostserv/request') }}">Request</a></li>
                                    @if (Session::get('hostserv'))
                                    <li><a href="{{ URL::to('hostserv/waiting') }}">Waiting</a></li>
                                    @endif
                                </ul>
                            </li>
                            <li class="divider-vertical"></li>
                        </ul>

                        @if (Session::get('operserv'))
                        <!-- main nav right -->
                        <ul class="nav pull-right">
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ ucfirst(Config::get('Atheme::default.atheme_services.operserv')) }}</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('operserv') }}">AKill</a></li>
                                    <li><a href="{{ URL::to('operserv/modules') }}">Modules</a></li>
                                    <li><a href="{{ URL::to('operserv/rehash') }}">Rehash</a></li>
                                    <li><a href="{{ URL::to('operserv/hash') }}">Password Hash</a></li>
                                </ul>
                            </li>
                            <li class="divider-vertical"></li>
                        </ul>
                        @endif
                    </div>
                    
                </div>
            </div>
        </div>
        