    <div class="container">

        <!-- user gravatar -->
        <div class="pull-left user-info">
            <img src="{{ Gravitas\API::url('jnewing@gmail.com', 30) }}" class="img-round" />
        </div>

        <!-- user options -->
        <div class="pull-left">
            <div class="btn-group">
                <a class="btn btn-primary" href="#"><i class="icon-user icon-white"></i> {{ Session::get('nickname') }}</a>
                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ URL::to('memoserv') }}"><i class="icon-pencil"></i> Memos</a></li>
                    <li><a href="{{ URL::to('nickserv/password') }}"><i class="icon-trash"></i> Change Password</a></li>
                    <li><a href="{{ URL::to('nickserv/email') }}"><i class="icon-ban-circle"></i> Change Email</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ URL::to('prefs') }} "><i class="icon-gear"></i> Prefrances</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ URL::to('logout') }}"><i class="i"></i> Logout</a></li>
                </ul>
            </div>

            <br />
        </div>

    </div>
