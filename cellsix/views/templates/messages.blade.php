        @if (Session::has('success'))
            <br />
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert" type="button">×</button>
                {{ Session::get('success') }}
            </div>
        @endif

        @if (Session::has('error'))
            <br />
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert" type="button">×</button>
                {{ Session::get('error') }}
            </div>
        @endif