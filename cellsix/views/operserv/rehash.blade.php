@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.operserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Rehash</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Rehash Atheme</h1>
                <p class="lead">Here service admins are able to rehash Atheme.</p>
                <hr />
            </div>
        </div>

        <!-- rehash block -->
        <div class="row">
            <div class="span12">
                <form action="" method="post">
                    <div class="form-horizontal well ">
                        <legend>Rehash</legend>
                        <p>Rehash Atheme.</p><br />

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Rehash</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

            });
        </script>

    @endsection
    