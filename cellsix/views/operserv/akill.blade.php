@layout('pages.fullpage')

    @section('content')

        <!-- confirmation modal -->
        <div id="cfModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cfModalLabel" aria-hidden="true">
            <form action="" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="cfModalLabel">Are you sure?</h3>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to remove this AKill?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Ooops No!</button>
                    <button type="submit" class="btn btn-primary btn-danger">Yes</button>
                </div>
                <input type="hidden" name="akill_id" value="" />
                <input type="hidden" name="action" value="remove" />
            </form>
        </div>

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.operserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>AKill</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Manage AKills</h1>
                <p class="lead">Manage existing AKills or add new ones using the form below.</p>
                <hr />
            </div>
        </div>
        
        <!-- existing akicks -->
        <div class="row">
            <div class="span12">
                <h2>AKills</h2>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="10">No.</th>
                            <th width="200">Nick / Host</th>
                            <th>Set By</th>
                            <th>Expires</th>
                            <th width="120">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($akills as $akill_entry => $akill_data)
                        <tr>
                            <td>{{ $akill_entry }}</td>
                            <td><span class="label">{{ $akill_data['host'] }}</span></td>
                            <td><a href="#">{{ $akill_data['setby'] }}</a></td>
                            <td><a href="#">{{ $akill_data['exp'] }}</a></td>
                            <td>
                                <a href="#cfModal" role="button" data-toggle="modal" id="confirm-{{ $akill_entry }}" class="btn btn-danger btn-mini" data-value="{{ $akill_entry }}"><icon class="icon-remove icon-white"></icon> Remove</a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">No AKills have been added.</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Add AKill</legend>
                        <p>Add an AKill.</p><br />

                        <div class="control-group">
                            <label class="control-label">Type</label>
                            <div class="controls">
                                <select name="akill_type">
                                    <option>Permanent</option>
                                    <option>Timed</option>
                                </select>
                            </div>
                        </div>

                        <div id="akill-timed" class="control-group hide">
                            <label class="control-label">Duration</label>
                            <div class="controls form-inline">
                                <input type="text" name="akill_duration" placeholder="10" />
                                <select name="akill_duration_type">
                                    <option value="">Minutes</option>
                                    <option value="h">Hours</option>
                                    <option value="d">Days</option>
                                    <option value="w">Weeks</option>
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Nick / Hostmask</label>
                            <div class="controls">
                                <input type="text" name="nickname" placeholder="Nickname / Hostmask" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Reason</label>
                            <div class="controls">
                                <input type="text" name="reason" placeholder="Reason for akick (Optional)" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Add AKill</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

                $('select[name="akill_type"]').change(function(e) {
                    if ($(this).val() == 'Timed') {
                        $('#akill-timed').show();
                    } else {
                        $('#akill-timed').hide();
                    }
                });

                $('a[id^="confirm"]').click(function(e) {

                    var akid = $(this).attr('data-value');
                    //$('#cfModal').modal('show');

                    $('input[name="akill_id"]').val(akid);
                });

            });
        </script>

    @endsection
    