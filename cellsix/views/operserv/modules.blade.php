@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.operserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Module Manager</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Manage Modules</h1>
                <p class="lead">Here service admins can load or unload modules.</p>
                <p><span class="label label-important">Warning!</span> It should be noted that unloading the wrong module can cause Atheme to crash! This is not Athemes fault but that of the end user. Be careful when unloading modules on a production server.</p>
                <hr />
            </div>
        </div>

        <!-- unload module -->
        <div class="row">
            
            <!-- unload form -->
            <div class="span6">
                <form action="" method="post">
                    <div class="form-horizontal well ">
                        <legend>Unload Module</legend>
                        <p>Unload a loaded module.</p><br />

                        <div class="control-group">
                            <label class="control-label">Module</label>
                            <div class="controls">
                                <select name="unload_module">
                                    @foreach ($modules as $mod)
                                    <option value="{{ $mod['module'] }}">{{ $mod['module'] . ' - ' . $mod['loaded'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Unload Module</button>
                        </div>
                    </div>
                <input type="hidden" name="action" value="unload" />
                </form>
            </div>

            <!-- load module -->
            <div class="span6">
                <form action="" method="post">
                    <div class="form-horizontal well ">
                        <legend>Load Module</legend>
                        <p>Load a module into Atheme.</p><br />

                        <div class="control-group">
                            <label class="control-label">Module</label>
                            <div class="controls">
                                <input type="text" name="load_module" placeholder="someservice/module" />
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Load Module</button>
                        </div>
                    </div>
                <input type="hidden" name="action" value="load" />
                </form>
            </div>

        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

            });
        </script>

    @endsection
    