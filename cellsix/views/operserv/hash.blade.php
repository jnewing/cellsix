@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.operserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Password Hash</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Password Hash</h1>
                <p class="lead">Hash a password using crypto methods that work with Atheme.</p>
                <hr />
            </div>
        </div>

        @if ($hash)
        <h3>Hash</h3>
        <pre>{{ $hash }}</pre>
        @endif

        <!-- rehash block -->
        <div class="row">
            <div class="span12">
                <form action="" method="post">
                    <div class="form-horizontal well ">
                        <legend>Hash</legend>
                        <p>Password hash.</p><br />

                        <div class="control-group">
                            <label class="control-label">Hashing Method</label>
                            <div class="controls">
                                <select name="enc_type">
                                    <option value="CRYPT">POSIX Crypt</option>
                                    <option value="MD5">MD5</option>
                                    <option value="SHA1">SHA1</option>
                                    <option value="SHA256">SHA256</option>
                                </select>
                            </div>
                        </div>                        

                        <div class="control-group">
                            <label class="control-label">Password</label>
                            <div class="controls">
                                <input type="text" name="password" placeholder="Your password" />
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Hash</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

            });
        </script>

    @endsection
    