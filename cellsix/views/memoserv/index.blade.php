@layout('pages.fullpage')

    @section('content')

        <!-- view modal -->
        <div id="viewModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="viewModalLabel">Memo</h3>
            </div>
            <div class="modal-body">
                <br />
                <p id="the-memo"></p>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </div>
        </div>

        <!-- reply modal -->
        <div id="replyModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="replyModalLabel" aria-hidden="true">
        <form action="{{ URL::to('memoserv/send') }}" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="replyModalLabel">Memo To</h3>
            </div>
            <div class="modal-body">
                <br />
                <div class="control-group">
                    <label class="control-label">To</label>
                    <div class="controls">
                        <input type="text" name="nickname" placeholder="Nickname" value="" class="input-xlarge" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Memo</label>
                    <div class="controls">
                        <textarea name="memo" placeholder="Memo..." style="width: 509px; height: 124px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary">Send Memo</button>
            </div>
        </form>
        </div>

        <!-- forward modal -->
        <div id="fwdModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="fwdModalLabel" aria-hidden="true">
        <form action="{{ URL::to('memoserv/forward/') }}" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="fwdModalLabel">Forward Memo</h3>
            </div>
            <div class="modal-body">
                <br />
                <div class="control-group">
                    <label class="control-label">To</label>
                    <div class="controls">
                        <input type="text" name="nickname" placeholder="Nickname" value="" class="input-xlarge" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary">Forward</button>
            </div>
        </form>
        </div>

        <!-- delete modal -->
        <div id="delModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
        <form action="{{ URL::to('memoserv/delete/') }}" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="delModalLabel">Delete Memo?</h3>
            </div>
            <div class="modal-body">
                <br />
                <p>Are you sure you wish to delete this memo? This action <strong>can not</strong> be undone.</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Oops No!</button>
                <button type="submit" class="btn btn-primary btn-danger">Delete</button>
            </div>
        </form>
        </div>

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li class="active">{{ ucfirst(Config::get('Atheme::default.atheme_services.memoserv')) }}</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Memos</h1>
                <p class="lead">Below you can manage your new and old memos.</p>
                <hr />
            </div>
        </div>
        
        <!-- current memos -->
        <div class="row">
            <div class="span12">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="150">From</th>
                            <th width="300">Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if (count($memos) > 0)
                        @foreach ($memos as $memo)
                        <tr>
                            <td>{{ ($memo['unread']) ? '<span class="label">NEW</span>' : '' }} {{ $memo['from'] }}</td>
                            <td>{{ $memo['sent'] }}</td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-small btn-view" data-value="{{ $memo['id'] }}">View</button>
                                    <button class="btn btn-small btn-reply" data-value="{{ $memo['from'] }}">Reply</button>
                                    <button class="btn btn-small btn-fwd" data-value="{{ $memo['id'] }}">Forward</button>
                                    <button class="btn btn-small btn-danger btn-delete" data-value="{{ $memo['id'] }}">Delete</button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">No memos :(</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="span4 offset8">
                <div class="btn-group pull-right">
                    <button class="btn btn-new">New Memo</button>
                    <a href="{{ URL::to('memoserv/delete/ALL') }}" class="btn btn-danger">Delete ALL</a>
                    <a href="{{ URL::to('memoserv/delete/OLD') }}" class="btn btn-danger">Delete Old</a>
                </div>
            </div>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {
                
                // view button
                $('.btn-view').click(function(e) {
                    var id = $(this).attr('data-value');
                    
                    // get the memo
                    $.ajax({
                        url: '{{ URL::to('memoserv/getmemo/') }}' + id,
                        data: {},
                        success: function(response) {
                            $('#the-memo').html(response.memo.replace(/\n/g, '<br />'));
                        },
                        dataType: 'json'
                    });

                    // toggle the modal
                    $('#viewModal').modal('toggle')
                });

                // reply button
                $('.btn-reply').click(function(e) {
                    var nickname = $(this).attr('data-value');

                    // set the reply user
                    $('input[name="nickname"]').val(nickname);

                    // toggle the modal
                    $('#replyModal').modal('toggle');
                });

                // forward button
                $('.btn-fwd').click(function(e) {
                    var id = $(this).attr('data-value');
                    var formURI = $('#fwdModal form').attr('action');

                    // setup our new form URI
                    $('#fwdModal form').attr('action', formURI + id);                    

                    // toggle our modal
                    $('#fwdModal').modal('toggle');
                });

                // delete button
                $('.btn-delete').click(function(e) {
                    var id = $(this).attr('data-value');
                    var formURI = $('#delModal form').attr('action');

                    // setup our new form URI
                    $('#delModal form').attr('action', formURI + id);

                    // toggle the confirmation
                    $('#delModal').modal('toggle');
                });

                // new memo button
                $('.btn-new').click(function(e) {
                    $('#replyModal').modal('toggle');
                });
            });
        </script>

    @endsection
    