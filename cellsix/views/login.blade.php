<!doctype html>
<html lang="en">
<head>
    <title>CellSix :: Atheme v4.0</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- css -->
    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/bootstrap-responsive.min.css') }}
    {{ HTML::style('css/cellsix.css') }}
    
    <!-- js -->
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    {{ HTML::script('js/bootstrap.js') }}
</head>
<body class="login">

    <!-- top nav -->
    <div class="navbar navbar-fixed-top top-line">
        <div class="container">
            <!-- logo -->
            <div class="pull-left">
                <a href="{{ URL::to('') }}" class="brand">Cell<strong>Six</strong><span class="label label-inverse">v4.0</span></a>
            </div>
        </div>
    </div>

    <!-- login box -->
    <div class="containter">
        <form action="" class="well login-form" method="post">
            <legend><icon class="icon-circles"></icon>User Login <icon class="icon-circles-reverse"></icon></legend>

            @include('templates.messages')

            @if (Session::has('errors.login'))
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert" type="button">×</button>
                    {{ Session::get('errors.login') }}
                </div>
            @endif

            <div class="control-group">
                <label class="control-label">Nickname</label>
                <div class="controls">
                    <div class="input-prepend">
                        <span class="add-on"><icon class="icon-user icon-cream"></icon></span>
                        <input type="text" name="nickname" class="input" placeholder="Nickname" />
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">Password</label>
                <div class="controls">
                    <div class="input-prepend">
                        <span class="add-on"><icon class="icon-password icon-cream"></icon></span>
                        <input class="input" type="password" id="password" name="password" placeholder="Password" />
                    </div>
                </div>
            </div>
            <div class="control-group signin">
                <div class="controls ">
                    <button type="submit" class="btn btn-block" id="">Sign in</button>
                    <div class="clearfix">
                        @if (Config::get('application.enable_web_register'))
                        <span class="icon-forgot"></span><a href="{{ URL::to('register') }}">Register new user.</a>
                        @endif
                    </div>
                </div>
            </div>
        </form>

    </div>
    
    <!-- footer -->
    <div class="footer lfooter" align="center">
        {{ "<p><strong>Cell Six</strong> Web Panel (Copyright &copy; Joseph Newing, 2011 - 2012)<br />A:" . Config::get('Atheme::default.atheme_bundle_ver') . " X:" . Config::get('XMLRPC::default.xmlrpc_bundle_ver') . " C:" . Config::get('application.cellsix_ver') . "</p>" }}
    </div>

</body>
</html>