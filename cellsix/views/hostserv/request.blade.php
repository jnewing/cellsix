@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.hostserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Request</li>
                </ul>
            </div>
        </div>

        @include('templates.messages')

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Request</h1>
                <p class="lead">Request a new Vhost.</p>
                <hr />
            </div>
        </div>
        
        <!-- block -->
        <div class="row">
            <!-- form -->
            <form action="" method="post">
                <div class="span12">
                    <div class="form-horizontal well ">
                        <legend>Request Vhost</legend>
                        <p>Users can submit a request for a custom vhost.</p><br />

                        <div class="control-group">
                            <label class="control-label">Vhost</label>
                            <div class="controls">
                                <input type="text" name="vhost" placeholder="request.some.vhost" class="input-xlarge" />
                            </div>
                        </div>

                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Make Request</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

            });
        </script>

    @endsection
    