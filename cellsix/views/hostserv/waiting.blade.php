@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.hostserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Waiting</li>
                </ul>
            </div>
        </div>

        <div id="messages">
        @include('templates.messages')
        </div>

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Manage Waiting VHosts</h1>
                <p class="lead">Manage vhosts that users have requested and are currently in the waiting queue.</p>
                <hr />
            </div>
        </div>
        
        <!-- existing akicks -->
        <div class="row">
            <div class="span12">
                <h2>Waiting</h2>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="200">Nickname</th>
                            <th>Vhost</th>
                            <th width="200">Req. Date</th>
                            <th width="120">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($waiting as $request)
                        <tr>
                            <td>{{ $request['nickname'] }}</td>
                            <td><span class="label">{{ $request['vhost'] }}</span></td>
                            <td><a href="#">{{ $request['date'] }}</a></td>
                            <td>
                                <a href="#" class="btn btn-mini btn-accept" data-value="{{ $request['nickname'] }}">Accept</a>
                                <a href="#" class="btn btn-danger btn-mini btn-reject" data-value="{{ $request['nickname'] }}">Reject</a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">No vhosts waiting in queue..</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

                // ajax vhost accept
                $('.btn-accept').click(function(e) {
                    e.preventDefault();
                    var row = $(this).parent().parent();

                    $.ajax({
                        type: 'POST',
                        url: '{{ URL::to('hostserv/waiting') }}',
                        
                        data: {
                            'action': 'accept',
                            'nickname': $(this).attr('data-value')
                        },

                        success: function(response) {
                            if (response.success == true) {
                                $('#messages').append('<br /> \
                                    <div class="alert alert-success"> \
                                        <button class="close" data-dismiss="alert" type="button">×</button> \
                                        ' + response.msg + '</div>');

                                $(row).hide();
                            } else {
                                $('#messages').append('<br /> \
                                    <div class="alert alert-error"> \
                                        <button class="close" data-dismiss="alert" type="button">×</button> \
                                        ' + response.msg + '</div>');                                
                            }
                        },

                        dataType: "json"
                    });
                });
                
                // ajax vhost reject
                $('.btn-reject').click(function(e) {
                    e.preventDefault();
                    var row = $(this).parent().parent();

                    $.ajax({
                        type: 'POST',
                        url: '{{ URL::to('hostserv/waiting') }}',

                        data: {
                            'action': 'reject',
                            'nickname': $(this).attr('data-value')
                        },

                        success: function(response) {
                            if (response.success == true) {
                                $('#messages').append('<br /> \
                                    <div class="alert alert-success"> \
                                        <button class="close" data-dismiss="alert" type="button">×</button> \
                                        ' + response.msg + '</div>');

                                $(row).hide();
                            } else {
                                $('#messages').append('<br /> \
                                    <div class="alert alert-error"> \
                                        <button class="close" data-dismiss="alert" type="button">×</button> \
                                        ' + response.msg + '</div>');                                
                            }
                        },

                        dataType: "json"

                    });
                });

            });
        </script>

    @endsection
    