@layout('pages.fullpage')

    @section('content')

        <!--  breadcrumb -->
        <div class="row hidden-phone">
            <div class="span12">
                <ul class="breadcrumb">
                    <li class="active">You are here: <icon class="icon-home icon-blue-l"></icon></li>
                    <li><a href="">{{ ucfirst(Config::get('Atheme::default.atheme_services.hostserv')) }}</a></li>
                    <li class="active"><span class="divider"></span>Offer List</li>
                </ul>
            </div>
        </div>

        <div id="msg">
            @include('templates.messages')
        </div>

        <!-- page heading  -->
        <div class="row">
            <div class="span12">
                <h1>Offer List</h1>
                <p class="lead">View a list of current HostServ offerings.</p>
                <hr />
            </div>
        </div>
        
        <!-- existing hostserv offer list -->
        @if (isset($hosts))
        <div class="row">
            <div class="span12">
                <h2>Offer List</h2>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>VHost</th>
                            <th width="120">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($hosts as $host)
                        <tr>
                            <td>{{ $host }}</td>
                            <td><a class="btn btn-take btn-mini" data-value="{{ $host }}"><icon class="icon-ok icon-white"></icon> Take</a></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="2">No vhosts are currently offered by HostServ</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        @endif

        <!-- page scripts -->
        <script type="text/javascript">
            $(function() {

                $('.btn-take').click(function(e) {
                    e.preventDefault();

                    $.ajax({
                        type: 'POST',
                        url: '{{ URL::to('hostserv') }}',
                        data: { 'vhost': $(this).attr('data-value') },
                        success: function(response) {
                            if (response.success) {
                                $('#msg').html('<br /><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">×</button>' + response.msg + '</div>');
                            } else {
                                $('#msg').html('<br /><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">×</button>' + response.msg + '</div>');
                            }
                        },
                        dataType: "json"
                    });
                });

            });
        </script>

    @endsection
    