@include('templates.header')

	@include('templates.navigation')

	<!-- content -->
	<div id="wrap">
		<div id="main" class="container">
		@yield('content')
		</div>
	</div>

@include('templates.footer')