<?php

return array(

	'ignore' => array(),
	'detail' => true,
	'log' => true,
	'logger' => function($exception)
	{
		Log::exception($exception);
	},

);