<?php

return array(

    // session driver
	'driver' => 'cookie',

	// todo remove this
	'table' => 'sessions',

	// session garbage collection probability
	'sweepage' => array(2, 100),

	// session lifetime
	'lifetime' => 60,

	// session expiratoin on close?
	'expire_on_close' => false,

	// session cookie name
	'cookie' => 'cellsix_session',

	// session cookie path
	'path' => '/',

	// session cookie domain
	'domain' => null,

	// https only session cookie>
	// this is currently a bad idea as Atheme does not yet support https
	'secure' => false,

);