<?php

return array(

	// application url without the trailing slash
	// example: http://services.mysite.com
	'url' => '',

	// base url for CellSix asset files.
	// leave this blank if you don't know what it is
	'asset_url' => '',

	// enable logging
	'logging' => true,

	// network name
	'network' => 'irc.cellsix.com',

	// enable or disable web registration
	'enable_web_register' => true,

	// if you use any form of mod_rewrite and want to remove
	// index.php from your URI then change this to blank.
	// NOTE: Once this has been set to '' you will need to create
	// some mod_rewrite (or equivilent) rules.
	//
	// This has been tested on Apache, Lighttpd (recommended) and Nginx ALL
	// of them currently work, however you'll have to write your own rules.
	// that being said see the readme.md for examples of each.
	'index' => 'index.php',

	// application key
	'key' => 'you_need_to_set_this!',

	// ssl links?
	// if your site supports ssl then by all means use it however know that Atheme's HTTP server
	// does not as of yet support SSL - *cough* go bug nenolod for this *cough*
	'ssl' => false,

	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	// __          __     _____  _   _ _____ _   _  _____ 
	// \ \        / /\   |  __ \| \ | |_   _| \ | |/ ____|
	//  \ \  /\  / /  \  | |__) |  \| | | | |  \| | |  __ 
	//   \ \/  \/ / /\ \ |  _  /| . ` | | | | . ` | | |_ |
	//    \  /\  / ____ \| | \ \| |\  |_| |_| |\  | |__| |
	//     \/  \/_/    \_\_|  \_\_| \_|_____|_| \_|\_____|
	//
	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
	// END OF USER CONFIGURATION. HERE BE DRAGONS!
	// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

	// profile
	'profiler' => false,

	// application encoding
	'encoding' => 'UTF-8',

	// default app language
	'language' => 'en',

	// supported languages
	'languages' => array(),

	// timezone
	'timezone' => 'UTC',

	'cellsix_ver' => 4.2,

	// aliases
	'aliases' => array(
		'Auth'       	=> 'Laravel\\Auth',
		'Authenticator' => 'Laravel\\Auth\\Drivers\\Driver',
		'Asset'      	=> 'Laravel\\Asset',
		'Autoloader' 	=> 'Laravel\\Autoloader',
		'Blade'      	=> 'Laravel\\Blade',
		'Bundle'     	=> 'Laravel\\Bundle',
		'Cache'      	=> 'Laravel\\Cache',
		'Config'     	=> 'Laravel\\Config',
		'Controller' 	=> 'Laravel\\Routing\\Controller',
		'Cookie'     	=> 'Laravel\\Cookie',
		'Crypter'    	=> 'Laravel\\Crypter',
		'DB'         	=> 'Laravel\\Database',
		'Eloquent'   	=> 'Laravel\\Database\\Eloquent\\Model',
		'Event'      	=> 'Laravel\\Event',
		'File'       	=> 'Laravel\\File',
		'Filter'     	=> 'Laravel\\Routing\\Filter',
		'Form'       	=> 'Laravel\\Form',
		'Hash'       	=> 'Laravel\\Hash',
		'HTML'       	=> 'Laravel\\HTML',
		'Input'      	=> 'Laravel\\Input',
		'IoC'        	=> 'Laravel\\IoC',
		'Lang'       	=> 'Laravel\\Lang',
		'Log'        	=> 'Laravel\\Log',
		'Memcached'  	=> 'Laravel\\Memcached',
		'Paginator'  	=> 'Laravel\\Paginator',
		'Profiler'  	=> 'Laravel\\Profiling\\Profiler',
		'URL'        	=> 'Laravel\\URL',
		'Redirect'   	=> 'Laravel\\Redirect',
		'Redis'      	=> 'Laravel\\Redis',
		'Request'    	=> 'Laravel\\Request',
		'Response'   	=> 'Laravel\\Response',
		'Route'      	=> 'Laravel\\Routing\\Route',
		'Router'     	=> 'Laravel\\Routing\\Router',
		'Schema'     	=> 'Laravel\\Database\\Schema',
		'Section'    	=> 'Laravel\\Section',
		'Session'    	=> 'Laravel\\Session',
		'Str'        	=> 'Laravel\\Str',
		'Task'       	=> 'Laravel\\CLI\\Tasks\\Task',
		'URI'        	=> 'Laravel\\URI',
		'Validator'  	=> 'Laravel\\Validator',
		'View'       	=> 'Laravel\\View',
	),

);
