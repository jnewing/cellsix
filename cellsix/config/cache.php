<?php

return array(

	'driver' => 'file',
	'key' => 'cellsix',
	'database' => array('table' => 'cellsix_cache'),
	'memcached' => array(
		array('host' => '127.0.0.1', 'port' => 11211, 'weight' => 100),
	),

);