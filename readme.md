# CellSix #
CellSix is a web interface for Atheme IRC Services. It's built on PHP and aimed to be clean and classy looking.

## Notes ##
CellSix was created by Joseph Newing (synmuffin).

Special thanks to Bertrum, Tappy, and the [http://paradoxirc.net/](http://paradoxirc.net/ "ParadoxIRC Network") for testing, ideas and feedback.

nenolod and Atheme developers for patches, fixes, and working with myself. [http://www.atheme.net/](http://www.atheme.net/ "Atheme IRC Platform")

## Requirements ##
These should all be installed and working on your system prior to attempting to install CellSix.

- PHP 5.4+
- PHP XML-RPC (php5-xmlrpc)

## Install ##

Note: It should be noted that only the public/ folder needs to be made web accessable. I would recommend setting the public/ folder as the document root for the vhost on your webserver.

- Download a copy of the source, and extract it to a desired location.
- FIRST thing you should do is open a web browser and navigate to the check.php file. (example: http://yousite.com/cellsix/public/check.php) this file will simply make sure you have all the things needed to run CellSix.
- chmod or chown the "storage/" folder so it's writable by your webserver.
- Edit the bundles / atheme / config / default.php file and change the settings accordingly, see the file itself for comments and futher instructions.
- Edit the cellsix / config / application.php file and change the settings accordingly. Again see the file itself for comments and futher instructions.

That's it your done.

## Cleaner URI's ##

If you would like to remove the index.php from your URI's you can do this on any webserver that supports some kind of mod_rewrite. CellSix has been tested on Apache2, Lighttpd and Nginx.

To do this you must change the 'index' => 'index.php', to 'index' => '', within the cellsix / config / application.php file and then configure your webserver accordingly.

Example of webserver configurations are provided below.

**Apache2**
	
	# Turning on the rewrite engine is necessary for the following rules and
	# features. "+FollowSymLinks" must be enabled for this to work symbolically.
	
	<IfModule mod_rewrite.c>
	Options +FollowSymLinks
	RewriteEngine On
	</IfModule>
	
	# For all files not found in the file system, reroute the request to the
	# "index.php" front controller, keeping the query string intact
	
	<IfModule mod_rewrite.c>
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^(.*)$ index.php/$1 [L]
	</IfModule>

**Lighttpd**

	$HTTP["host"] =~ "example.com$" {
	        server.document-root = "/var/www/vhosts/example.com/public/"
	        accesslog.filename = "/var/www/vhosts/example.com/logs/access.log"
	
	        alias.url = ()
	        url.redirect = ()
	        url.rewrite-once = (
	                "^/(css|img|js)/.*\.(jpg|jpeg|gif|png|swf|avi|mpg|mpeg|mp3|flv|ico|css|js)$" => "$0",
	                "^/(favicon\.ico|robots\.txt|sitemap\.xml)$" => "$0",
	                "^/[^\?]*(\?.*)?$" => "index.php/$1"
	        )
	}

**Nginx**

	server {
		server_name .your.server;
		root /home/user/www/cellsix/public;
	
		index index.php index.html;
	
		#browse folders if no index file
	    	autoindex on; 
	
	  	# serve static files directly
	 	location ~* \.(jpg|jpeg|gif|css|png|js|ico|html)$ {
			access_log off;
	  		expires max;
		}
	
		# removes trailing slashes (prevents SEO duplicate content issues)
		if (!-d $request_filename)
		{
			rewrite ^/(.+)/$ /$1 permanent;
		} 
	
	    # canonicalize codeigniter url end points
	    # if your default controller is something other than "welcome" you should change the following
	    if ($request_uri ~* ^(/lobby(/index)?|/index(.php)?)/?$)
	    {
	        rewrite ^(.*)$ / permanent;
	    }
	 
	    # removes trailing "index" from all controllers
	    if ($request_uri ~* index/?$)
	    {
	        rewrite ^/(.*)/index/?$ /$1 permanent;
	    }
	
	    # unless the request is for a valid file (image, js, css, etc.), send to bootstrap
	    if (!-e $request_filename)
	    {
	        rewrite ^/(.*)$ /index.php?/$1 last;
	        break;
	    }
	 
	    # catch all
	    error_page 404 /index.php;
	
	        location ~ \.php$ {
			try_files $uri =404;
	                fastcgi_pass  unix:/tmp/php.socket;
	                fastcgi_index index.php;
	                #include fastcgi_params;
					include /home/user/www/nginx/fastcgi_params;
	        }
	        access_log /home/user/www/cellsix/storage/logs.access.log;
	        error_log  /home/user/www/cellsix/storage/logs.error.log;
	}

