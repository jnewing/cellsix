<?php return array(
    'atheme' => array(
        'autoloads' => array(
            'map' => array(
                'Atheme\\AthemeAPI' => '(:bundle)/athemeapi.php',
            ),
        ),
    ),
);