<?php namespace Atheme;

/**
 * Copyright (c) 2012, Joseph Newing
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Joseph Newing.
 * 
 * 4. Neither the name of the Joseph Newing nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * AthemeAPI Class
 * 
 * Class provides and API to Atheme via the XML-RPC transport.
 *
 * @category Atheme - API
 * @package CellSix
 * @author Joseph Newing <jnewing@gmail.com>
 * @license BSD
 * @copyright 2012 Joseph Newing
 * 
 */

use \Config, \Session, \XMLRPC;

class AthemeAPI
{
    public static $response;

    public static $authcookie;

    private static $_instance = NULL;

    /**
     * getInstance()
     * returns an instance of this class so we can use the Singleton pattern
     */
    public static function getInstance()
    {
        if (self::$_instance === null)
            self::$_instance = new self;

        return self::$_instance;
    }

    /**
     * login()
     * allows the user access to the atheme.login method
     *
     * @param string $username  - username of the person we are loggin in
     * @param string $password  - the password of the person we are loggin in
     * @return bool
     */
    public static function login($username, $password)
    {
        // try and login
        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.login', array($username, $password));

        if (is_array(self::$response))
            return FALSE;
        else
            return self::$response;
    }

    /**
     * register()
     * register function does just that, allows users to register with atheme.
     * 
     * @param string $nickname  - nickname of the registering user
     * @param string $password  - password of the registering user
     * @param string $email     - email of the registering user
     * @return object           - returns an instance of this class
     */
    public static function register($nickname, $password, $email)
    {
        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.command', array_merge(array('*', '*', '*', Config::get("Atheme::default.atheme_services.nickserv")), array('register', $nickname, $password, $email)));

        return self::getInstance();
    }

    /**
     * check()
     * uses atheme.isvalid method to check and make sure the passes authcookie is valid
     * @return bool
     */
    public static function check()
    {
        // here is hoping that Atheme devs either take the patch i've provided or do something similar to
        // the isvalid 
        // self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.isvalid', array(Session::get('authCookie'), Session::get('nickname')));

        // if (is_array(self::$response))
        //     return FALSE;
        // else
        //     return TRUE;

        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.command', array_merge(array(\Session::get('authCookie'), \Session::get('nickname'), \Request::ip(), Config::get("Atheme::default.atheme_services.nickserv")), array('help')));

        if (is_array(self::$response))
        {
            if (self::$response['faultString'] == 'Invalid authcookie for this account.' || self::$response['faultString'] == 'Unknown user.')
                return false; 
        }
        else
        {
            if (self::$response === 'Invalid authcookie for this account.')
                return false;
        }

        return true;
    }

    /**
     * __callStatic()
     * overloaded method to call the XML-RPC
     *
     * @param string $name      - name of the service
     * @param string $params    - parameters
     * @return mixed            - XML-RPC response
     */
    public static function __callStatic($name, $params)
    {
        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.command', array_merge(array(\Session::get('authCookie'), \Session::get('nickname'), \Request::ip(), Config::get("Atheme::default.atheme_services.{$name}")), $params));

        return self::getInstance();
    }

    /**
     * accesslist()
     * function will get a list of channels and the access flags user currently has access to
     *
     * @return array
     */
    public static function accesslist()
    {
        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.command', array_merge(array(\Session::get('authCookie'), \Session::get('nickname'), \Request::ip(), Config::get("Atheme::default.atheme_services.nickserv")), array('listchans')));

        $access_list = array();

        foreach (explode("\n", self::$response) as $line)
        {
            if (preg_match('/^Access flag\(s\)/m', $line))
            {
                $buffer = explode(" ", $line);
                $access_list[$buffer[4]] = $buffer[2];
            }
        }

        return $access_list;
    }

    /**
     * operaccess()
     * function checks to see if the user is able to issue a help command on operserv (really checking to see if they have access to any level to operserv)
     */
    public static function operaccess()
    {
        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.command', array_merge(array(\Session::get('authCookie'), \Session::get('nickname'), \Request::ip(), Config::get("Atheme::default.atheme_services.operserv")), array('help')));

        if (is_array(self::$response))
        {
            if (array_key_exists('faultString', self::$response))
                return false;
        }

        return true;
    }

    /**
     * hostaccess()
     * function checks to see if the user has access to the HostServ waiting command, if the user does it allows them to accept or reject waiting HostServ
     * user requests
     */
    public static function hostaccess()
    {
        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.command', array_merge(array(\Session::get('authCookie'), \Session::get('nickname'), \Request::ip(), Config::get("Atheme::default.atheme_services.hostserv")), array('waiting')));

        if (is_array(self::$response))
        {
            if (array_key_exists('faultString', self::$response))
                return false;
        }

        return true;
    }

    /**
     * newmessages()
     * function will return the number of new messages a user has
     */
    public static function newmessages()
    {
        self::$response = \XMLRPC\XML_RPC::CallMethod(Config::get('Atheme::default.atheme_host'), 'atheme.command', array_merge(array(\Session::get('authCookie'), \Session::get('nickname'), \Request::ip(), Config::get("Atheme::default.atheme_services.memoserv")), array('list')));

        if (preg_match('/\(([0-9]{0,100}) new\)./m', self::$response, $regs))
            return $regs[1];

        return 0;
    }

    /**
     * asString()
     * returns self::$response as a string
     */
    public static function asString()
    {
        if (is_array(self::$response))
        {
            if (array_key_exists('faultString', self::$response))
                return trim(self::$response['faultString']);

            return implode(" ", self::$response);
        }

        return trim(self::$response);
    }

    /**
     * asObject()
     * returns self::$response as an object
     */
    public static function asObject()
    {
        $obj = new \stdClass;

        if (is_array(self::$response))
        {
            if (array_key_exists('faultString', self::$response))
            {
                $obj->success = false;
                $obj->msg = self::$response['faultString'];

                return $obj;
            }

            $obj->success = true;
            $obj->msg = implode(" ", self::$response);

            return $obj;
        }

        $obj->success = true;
        $obj->msg = self::$response;

        return $obj;
    }

    /**
     * asDefault()
     * returns self::$response as it's default type
     */
    public static function asDefault()
    {
        return self::$response;
    }

    /**
     * channelName()
     * simple function that just makes sure a channel name has it's prepending #
     */
    public static function channelName($string)
    {
        if (substr($string, 0, 1) !== '#')
            return '#' . $string;

        return $string;
    }

    /**
     * asEntryList()
     * treats the respons as if it was a entry list
     */
    public static function asEntryList()
    {
        if (is_array(self::$response))
            if (array_key_exists('faultString', self::$response))
                return trim(self::$response['faultString']);

        $ret_array = array();

        foreach(explode("\n", self::$response) as $line)
        {
            if (preg_match('/^([0-9]{1,100}):/m', $line, $fregs))
            {
                preg_match('/^[0-9]{1,100}: (.*) \((.*)\) \[(.*)\]/m', $line, $regs);
                $ret_array[$fregs[1]] = array(
                    'host' => $regs[1],
                    'reason' => $regs[2],
                    'ext' => $regs[3]
                );
            }
                
        }

        return $ret_array;
    }

    /**
     * asAKillList()
     * function formats a list taken from AKILL LIST as it's different than all the others lists
     * perhaps something for Atheme developers would be to standerize the list format be it : : or something
     */
    public static function asAKillList()
    {
        // if (preg_match('/^[0-9]{1,100}: (.*) - by (.*) - (.*)/m', $subject, $regs)) {
        //     $result = $regs[1];
        // } else {
        //     $result = "";
        // }
        if (is_array(self::$response))
            if (array_key_exists('faultString', self::$response))
                return trim(self::$response['faultString']);

        $ret_array = array();

        foreach(explode("\n", self::$response) as $line)
        {
            if (preg_match('/^([0-9]{1,100}):/m', $line, $fregs))
            {
                preg_match('/^[0-9]{1,100}: (.*) - by (.*) - (.*)/m', $line, $regs);
                $ret_array[$fregs[1]] = array(
                    'host' => $regs[1],
                    'setby' => $regs[2],
                    'exp' => $regs[3]
                );
            }
        }

        return $ret_array;
    }

    /**
     * asMemoList()
     * treats the response as if it was a list of memos for a user.
     */
    public static function asMemoList()
    {
        if (is_array(self::$response))
            if (array_key_exists('faultString', self::$response))
                return trim(self::$response['faultString']);

        $ret_array = array();

        foreach(explode("\n", self::$response) as $line)
        {
            if (preg_match('/- ([0-9]{0,100}) From: (.*) Sent: (.*)/m', $line, $regs))
            {
                $ret_array[] = array(
                    'id' => $regs[1],
                    'from' => $regs[2],
                    'sent' => $regs[3],
                    'unread' => (preg_match('/\[unread\]/m', $line)) ? true : false
                );
            }    
        }

        return $ret_array;
    }

    /**
     * asHostList()
     * function will treat the respons as an array of host listings, offered to the user.
     */
    public static function asHostList()
    {
        if (is_array(self::$response))
            if (array_key_exists('faultString', self::$response))
                return trim(self::$response['faultString']);

        $ret_array = array();

        foreach (explode("\n", self::$response) as $line)
        {
            if (preg_match('/vhost:(.*), /m', $line, $regs))
                $ret_array[] = $regs[1];
        }

        return $ret_array;
    }

    /**
     * asModuleList()
     * function will format the operserv/modlist command to by parsing it into an array.
     */
    public static function asModuleList()
    {
        if (is_array(self::$response))
            if (array_key_exists('faultString', self::$response))
                return trim(self::$response['faultString']);

        $ret_array = array();

        foreach (explode("\n", self::$response) as $line)
        {
            if (preg_match('/^([0-9]{1,10}): (.*) \[(.*)\]/m', $line, $regs))
                $ret_array[] = array(
                    'id' => $regs[1],
                    'module' => trim($regs[2]),
                    'loaded' => $regs[3]
                );
        }

        return $ret_array;
    }

    /**
     * asWaitingList()
     * function will format the output as if it were a HostServ waiting list.
     */
    public static function asWaitingList()
    {
        if (is_array(self::$response))
            if (array_key_exists('faultString', self::$response))
                return trim(self::$response['faultString']);

        $ret_array = array();

        foreach (explode("\n", self::$response) as $line)
        {
            if (preg_match('/^Nick:(.*), vhost:(.*) \(.* - (.*)\)/m', $line, $regs))
            {
                $ret_array[] = array(
                    'nickname' => $regs[1],
                    'vhost' => $regs[2],
                    'date' => $regs[3]
                );
            }
        }

        return $ret_array;
    }

}
