<?php

/**
 * Atheme server configuration
 */

return array(

    // hostname or ip address of the Atheme XML-RPC server
    'atheme_host' => 'localhost',

    // the port Atheme XML-RPC server is running on
    'atheme_port' => 8080,

    // path to xmlrpc HTTP
    'atheme_path' => '/xmlrpc',

    // Atheme version
    'atheme_version' => 'atheme/7',

    // Atheme service names
    'atheme_services' => array(
        'nickserv' => 'nickserv',
        'chanserv' => 'chanserv',
        'memoserv' => 'memoserv',
        'hostserv' => 'hostserv',
        'operserv' => 'operserv',
    ),

    // atheme bundle version
    'atheme_bundle_ver' => 2.3,

);