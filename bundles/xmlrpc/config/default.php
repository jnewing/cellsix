<?php

/**
 * XML-RPC client configuration
 */

return array(
	// xmlrpc useragent
    'xmlrpc_useragent'  => 'CellSix/1.3',

    // xml bundle version
    'xmlrpc_bundle_ver' => 2.3,
);