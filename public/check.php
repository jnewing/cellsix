<!doctype html>
<html lang="en">
<head>
	<title>CellSix :: System Check</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- css -->
	<style>
	.body { background: #fff; margin: 0 auto; text-align: center; }
	div#check { margin: auto; width: 500px; }
	.pass { color: green; }
	.fail { font-weight: bold; color: red; }


	html, body, form, fieldset, pre, blockquote, ul, ol, dl, address {
		margin: 0;
		padding: 0;
		font: 1em Arial, Tahoma, sans-serif;
		color: #696969;
	}

	h1, h2, h3, h4 { color: #1E1E1E; }
	h1 { font-size: 1.3em; }
	h2 { font-size: 1.2em; }
	h3 { font-size: 1.1em; }
	h4 { font-size: 1.0em; }
	
	a:link {
		color: #d42945;
		text-decoration: none;
		border-bottom: 1px dotted #ffbac8;
	}	
	
	a:visited {
		color: #d42945;
		border-bottom: none;
		text-decoration: none;
	}		
	
	a:hover,
	a:focus {
		color: #f03b58;
		border-bottom: 1px solid #f03b58;
		text-decoration: none;
	}
	
	table a, table a:link, table a:visited { border: none; }
	img { border: 0; margin-top: .5em; }
	
	table {
		width: 100%;
		border-top: 1px solid #e5eff8;
		border-right: 1px solid #e5eff8;
		margin: 1em auto;
		border-collapse: collapse;
	}
	
	caption {
		color: #9ba9b4;
		font-size: .94em;
		letter-spacing: .1em;
		margin: 1em 0 0 0;
		padding: 0;
		caption-side: top;
		text-align: center;
	}	
	
	tr.odd td { background: #f7fbff; }
	tr.odd .column1	{ background:#f4f9fe; }	
	.column1 { background:#f9fcfe; }
	
	td {
		color: #678197;
		border-bottom: 1px solid #e5eff8;
		border-left: 1px solid #e5eff8;
		padding: .3em 1em;
		text-align: center;
	}

	th {
		font-weight: normal;
		color: #678197;
		text-align: left;
		border-bottom: 1px solid #e5eff8;
		border-left: 1px solid #e5eff8;
		padding: .3em 1em;
	}

	thead th {
		background: #f4f9fe;
		text-align: center;
		color: #66a3d3
	}

	tfoot th {
		text-align:center;
		background:#f4f9fe;
	}
	
	tfoot th strong {
		margin:.5em .5em .5em 0;
		color: #66a3d3;
	}

	tfoot th em {
		color: #f03b58;
		font-weight: bold;
		font-size: 1.1em;
		font-style: normal;
	}
	</style>
</head>
<body>

	<div id="check">
		<table width="100%">
			<caption>Few checks to make sure we have everything we need.</caption>
			<thead>
				<tr class="odd">
					<th scope="col">Check</th>
					<th scope="col">Status</th>
				</tr>	
			</thead>
			<tbody>
				<tr>
					<td>PHP Version >= 5.4.0</td>
					<td><?php if (version_compare(PHP_VERSION, '5.4.0') >= 0) : ?> <span class="pass"><?php echo phpversion(); ?></span> <?php else: ?> <span class="fail"><?php echo phpversion(); ?></span><?php endif; ?></td>
				</tr>
				<tr class="odd">
					<td>PHP XML-RPC (php5-xmlrpc)</td>
					<td><?php if (function_exists('xmlrpc_encode_request')) : ?> <span class="pass">Passed.</span> <?php else: ?> <span class="fail">Failed! (Missing php5-xmlrpc)</span> <?php endif; ?></td>
				</tr>
				<tr>
					<td>Writable ../storage</td>
					<td><?php if (is_writable('../storage')) : ?> <span class="pass">Passed.</span> <?php else: ?> <span class="fail">Failed!</span> <?php endif; ?></td>
				</tr>
			</tbody>
		</table>

		<hr />

		<p>After all the above checks passed! CellSix should work.</p>

	</div>
</body>
</html>